package me.jakev.oldscanner;

import api.common.GameServer;
import api.listener.Listener;
import api.listener.events.entity.EntityScanEvent;
import api.mod.StarLoader;
import api.mod.StarMod;
import org.schema.game.common.data.player.AbstractOwnerState;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.world.Universe;
import org.schema.game.server.data.GameServerState;

/**
 * Created by Jake on 6/18/2021.
 * <insert description here>
 */
public class OldScanner extends StarMod {
    @Override
    public void onEnable() {
        System.err.println("[OldScanner] Enabled");
        StarLoader.registerListener(EntityScanEvent.class, this, new Listener<EntityScanEvent>() {
            @Override
            public void onEvent(EntityScanEvent event) {
                AbstractOwnerState ownerState = event.getOwner();
                if(ownerState == null){
                    // Npc or something
                    // System.err.println("[OldScanner] Owner state null, not scanning. <Exception prevented>");
                }else{
                    Universe universe = GameServerState.instance.getUniverse();
                    if(universe == null){
                        System.err.println("[OldScanner] Universe is null, not scanning <Exception prevented>");
                    }else{
                        GameServer.getServerState().scanOnServer(null, (PlayerState) ownerState);
                    }
                }
            }
        });
    }
}
