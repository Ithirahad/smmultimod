package me.jakev.customrefinerytest;

import api.config.BlockConfig;
import api.mod.StarMod;
import api.utils.element.CustomModRefinery;
import api.utils.element.recipe.FixedRecipeBuilder;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.element.FixedRecipe;

public class CustomRefineryTest extends StarMod {
    @Override
    public void onEnable() {

    }

    @Override
    public void onBlockConfigLoad(BlockConfig config) {
        FixedRecipe recipies = new FixedRecipeBuilder()
                .input(ElementKeyMap.CORE_ID, 1).output(ElementKeyMap.TERRAIN_ICE_ID, 5).next()
                .input(ElementKeyMap.GLASS_ID, 1).output(ElementKeyMap.THRUSTER_ID, 100).build();

        //Custom properties for our mod refinery
        CustomModRefinery modRefinery = new CustomModRefinery(recipies, "RefineryName", "Production Text", 1000);

        //Register the refinery
        ElementInformation info = BlockConfig.newRefinery(this, "MyEpicRefinery", new short[]{44}, modRefinery);
        BlockConfig.add(info);
    }
}
