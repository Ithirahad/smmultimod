package me.jakev.eelite.particles.shipexplode;

import api.ModPlayground;
import api.utils.particle.ModParticle;
import api.utils.particle.ModParticleUtil;

/**
 * Created by Jake on 12/9/2020.
 * <insert description here>
 */
public class InvisibleEmitterParticle extends ModParticle {

    private static final float r = 0.005F;
    private final int spr;
    float rx = ModPlayground.randFloat(-r,r);
    float ry = ModPlayground.randFloat(-r,r);
    float rz = ModPlayground.randFloat(-r,r);

    public InvisibleEmitterParticle(int spr) {
        this.spr = spr;
    }

    @Override
    public void spawn() {
//        colorA = 0;
        sizeX = 10;
        sizeY = 10;
        super.spawn();
    }

    @Override
    public void update(long currentTime) {
        this.velocity.x += rx;
        this.velocity.y += ry;
        this.velocity.z += rz;
        this.velocity.scale(0.999F);

            DebrisFlairParticle dfp = new DebrisFlairParticle();
            ModParticleUtil.playClient(sectorId, this.position, spr, dfp);
    }
}
