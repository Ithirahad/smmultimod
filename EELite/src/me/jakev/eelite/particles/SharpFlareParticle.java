package me.jakev.eelite.particles;

import api.utils.particle.ModParticle;

/**
 * Created by Jake on 1/28/2022.
 * <insert description here>
 */
public class SharpFlareParticle extends ModParticle {
    private float sizeGrow;

    public SharpFlareParticle() {
    }

    @Override
    public void spawn() {
    }

    @Override
    public void update(long currentTime) {
        fadeOverTime(this, currentTime);
    }
}
