package me.jakev.eelite.particles;

import api.utils.particle.ModParticle;

/**
 * Created by Jake on 1/28/2022.
 * <insert description here>
 */
public class FlashParticle extends ModParticle {
    private float sizeGrow;

    public FlashParticle(float sizeGrow) {
        this.sizeGrow = sizeGrow;
    }

    @Override
    public void spawn() {
        sizeX = sizeGrow;
        sizeY = sizeGrow;
    }

    @Override
    public void update(long currentTime) {
        if (ticksLived < 3) {
            sizeX += sizeGrow;
            sizeY += sizeGrow;
        } else {
            sizeX -= 3;
            sizeY -= 3;
            if (sizeX < 0) {
                markForDelete();
            }
        }
    }
}
