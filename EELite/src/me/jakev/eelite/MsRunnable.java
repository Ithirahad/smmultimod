package me.jakev.eelite;

import api.mod.StarMod;
import api.utils.StarRunnable;

/**
 * Created by Jake on 10/23/2021.
 * <insert description here>
 */
public abstract class MsRunnable extends StarRunnable {
    private long ms;
    private long lastMs = 0;

    public MsRunnable(StarMod mod, int ms){
        this.ms = ms;
        runTimer(mod, 1);
    }
    @Override
    public void run() {
        long delta = System.currentTimeMillis() - lastMs;
        if(delta > ms){
            lastMs = System.currentTimeMillis();
            runMs();
        }
    }
    public abstract void runMs();
}