package me.jakev.eelite.listener;

import api.listener.Listener;
import api.listener.events.weapon.BeamPostAddEvent;
import api.mod.StarLoader;
import api.mod.StarMod;
import api.utils.StarRunnable;
import api.utils.particle.ModParticle;
import api.utils.particle.ModParticleUtil;
import me.jakev.eelite.EFXSprites;
import me.jakev.eelite.particles.ExpandingRingParticle;
import me.jakev.eelite.particles.FadeScaleParticle;
import org.schema.game.common.controller.elements.beam.damageBeam.DamageBeamHandler;

import javax.vecmath.Vector3f;

/**
 * Created by Jake on 1/28/2022.
 * <insert description here>
 */
public class EFXBeamListener {
    public static int ran = 0;
    public static void init(StarMod mod) {
        new StarRunnable(){
            @Override
            public void run() {
                ran++;
            }
        }.runTimer(mod, 1);
        StarLoader.registerListener(BeamPostAddEvent.class, new Listener<BeamPostAddEvent>() {
            @Override
            public void onEvent(BeamPostAddEvent event) {

                if (!(event.getHandler() instanceof DamageBeamHandler)) {
                    return;
                }
                Vector3f start = new Vector3f(event.getBeamState().from);

                Vector3f normal = new Vector3f();
                Vector3f to = new Vector3f();
                if (event.getBeamState().hitPoint == null) {
                    normal.set(event.getBeamState().to);
                    to.set(event.getBeamState().to);
                } else {
                    normal.set(event.getBeamState().hitPoint);
                    to.set(event.getBeamState().hitPoint);

                }
                normal.sub(event.getBeamState().from);
                float length = normal.length();
                normal.normalize();

                Vector3f inverseNormal = new Vector3f(normal);
                inverseNormal.scale(-0.1F);

                if(event.getHandler().getBeamShooter() == null) return;
                int secId = event.getHandler().getBeamShooter().getSectorId();
                if(ran%2==0){
                    ExpandingRingParticle particle = new ExpandingRingParticle(0.2f, 30);
                    particle.lifetimeMs = 1700;
                    particle.velocity = inverseNormal;

                    Vector3f particleDir = new Vector3f(inverseNormal);
                    particleDir.normalize();
                    particle.setArbitraryNormalOverride(new Vector3f(particleDir));

                    ModParticle.setColorF(particle, event.getBeamState().color);
                    ModParticleUtil.playClient(secId, to, EFXSprites.RING1.getSprite(), particle);
                }

                normal.scale(25);
                start.add(normal);
                start.add(normal);

                FadeScaleParticle particle = new FadeScaleParticle(10, 2, 0);
                particle.lifetimeMs = 700;
                particle.velocity = normal;

                particle.startColor.set(event.getBeamState().color);
                particle.endColor.set(event.getBeamState().color);

                Vector3f particleDir = new Vector3f(normal);
                particleDir.normalize();
                particle.setArbitraryNormalOverride(new Vector3f(particleDir));

                ModParticleUtil.playClient(secId, start, EFXSprites.RING1.getSprite(), particle);

            }
        }, mod);
    }
}
