package me.jakev.eelite.listener;

import api.common.GameCommon;
import api.listener.Listener;
import api.listener.events.weapon.CannonProjectileAddEvent;
import api.mod.StarLoader;
import api.utils.particle.ModParticleUtil;
import me.jakev.eelite.EFXLite;
import me.jakev.eelite.EFXSprites;
import me.jakev.eelite.particles.ExpandingRingParticle;
import me.jakev.eelite.particles.SharpFlareParticle;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.damage.Damager;
import org.schema.schine.network.Identifiable;

import javax.vecmath.Matrix3f;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;

/**
 * Created by Jake on 1/28/2022.
 * <insert description here>
 */
public class EFXCannonListener {
    public static void init(EFXLite mod){
        StarLoader.registerListener(CannonProjectileAddEvent.class, new Listener<CannonProjectileAddEvent>() {
            @Override
            public void onEvent(CannonProjectileAddEvent event) {
                if (event.isServer()) {
                    return;
                }
                final Vector3f dir = new Vector3f();
                Vector3f pos = new Vector3f();
                int ownerId = event.getContainer().getOwnerId(event.getIndex());
                if (!(GameCommon.getGameObject(ownerId) instanceof SegmentController)) {
                    return;
                }
                int SCALE = 20;
                event.getContainer().getPos(event.getIndex(), pos);
                event.getContainer().getVelocity(event.getIndex(), dir);
                Vector4f color = new Vector4f();
                event.getContainer().getColor(event.getIndex(), color);

                Vector3f originPos = new Vector3f(pos);

                dir.normalize();

                Vector3f offset = new Vector3f(dir);
                offset.scale(SCALE/2F);
                pos.add(offset);

                Vector3f normalA = new Vector3f(1,0,0);
                Vector3f normalB = new Vector3f(0,1,0);
                normalA.cross(normalA, dir);
                normalB.cross(normalB, dir);

                Matrix3f w = new Matrix3f();
                Vector3f normalOvr;
                if(normalA.dot(dir) < normalB.dot(dir)){
                    normalOvr = normalA;
                }else{
                    normalOvr = normalB;
                }

                dir.normalize();
                w.setColumn(1, dir);
                normalOvr.normalize();
                w.setColumn(2, normalOvr);

                //completely useless cause forward is unused
                Vector3f fwd = new Vector3f();
                fwd.cross(normalOvr, dir);
                fwd.normalize();
                w.setColumn(0, fwd);

                Identifiable owner = GameClientState.instance.getLocalAndRemoteObjectContainer().getLocalObjects().get(ownerId);
                if(owner instanceof Damager){
                    int sectorId = ((Damager) owner).getSectorId();
                    for (int i = 0; i < 2; i++) {
                        SharpFlareParticle particle = new SharpFlareParticle();
                        particle.lifetimeMs = 50;
                        particle.sizeX = 2F;
                        particle.sizeY = SCALE;
                        particle.normalOverride = w;
                        if(i == 0){
                            Matrix3f m = new Matrix3f(w);
                            m.setColumn(2, fwd);
                            particle.normalOverride = m;
                        }
                        ModParticleUtil.playClient(sectorId, pos, EFXSprites.FLARE.getSprite(), particle);
                    }
                    dir.scale(0.5F);
                    originPos.add(dir);
                    dir.scale(10F);
                    for (int i = 0; i < 3; i++) {
                        ExpandingRingParticle particle = new ExpandingRingParticle(0, 5F - i);
                        particle.lifetimeMs = 300;

                        particle.setArbitraryNormalOverride(dir);

//                        ModParticle.setColorF(particle, color);
                        ModParticleUtil.playClient(sectorId, originPos, EFXSprites.RING1.getSprite(), particle);
                        originPos.add(dir);
                    }

                }

            }
        }, mod);
    }
}
