package me.jakev.eelite;

import api.utils.particle.ModParticleUtil;

import javax.imageio.ImageIO;
import java.io.IOException;

/**
 * Created by Jake on 1/28/2022.
 * <insert description here>
 */
public enum EFXSprites {
    BALL, RING1, FLASH, FLARE,

    BIGSMOKE, FIREFLASH2, NOTHING2, REDBALL;

    private int sprite;
    private String name;

    public static void init(EFXLite mod, ModParticleUtil.LoadEvent event) {
        for (EFXSprites value : EFXSprites.values()) {
            String name = value.name().toLowerCase();
            value.name = name;
            try {
                value.sprite = event.addParticleSprite(ImageIO.read(mod.getJarResource("me/jakev/eelite/res/" + name + ".png")), mod);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public int getSprite() {
        return sprite;
    }

    public String getName() {
        return "efxlite_" + name;
    }
}
