package me.jakev.eelite;

import api.mod.StarMod;
import api.network.packets.PacketUtil;
import api.utils.particle.ModParticleUtil;
import me.jakev.eelite.listener.*;
import me.jakev.eelite.net.PacketSCOverheatEvent;
import me.jakev.eelite.net.PacketSCPlayExplosionParticle;

/**
 * Created by Jake on 1/28/2022.
 * <insert description here>
 */
public class EFXLite extends StarMod {
    public static StarMod inst;

    @Override
    public void onLoadModParticles(ModParticleUtil.LoadEvent event) {
        EFXSprites.init(this, event);
    }

    @Override
    public void onEnable() {
        inst = this;
        EFXBarListener.init(this);
        EFXMissileListener.init(this);
        EFXBeamListener.init(this);
        EFXCannonListener.init(this);
        EFXSoundListener.init(this);
        EFXExplosionListener.init(this);
        PacketUtil.registerPacket(PacketSCPlayExplosionParticle.class);
        PacketUtil.registerPacket(PacketSCOverheatEvent.class);
    }
}
