package me.jakev.turrethotkey;

import api.utils.game.PlayerUtils;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.data.PlayerControllable;
import org.schema.game.common.controller.SegmentController;
import org.schema.schine.graphicsengine.core.DrawableScene;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.shader.Shader;
import org.schema.schine.graphicsengine.shader.Shaderable;
import org.schema.schine.input.InputState;

/**
 * Created by Jake on 12/15/2020.
 * <insert description here>
 */
public class THKHudElement extends GUIElement implements Shaderable {

    public THKHudElement(InputState inputState) {
        super(inputState);
    }

    @Override
    public void cleanUp() {

    }

    @Override
    public void draw() {
        PlayerControllable control = PlayerUtils.getCurrentControl(GameClientState.instance.getPlayer());
        if(control instanceof SegmentController){
            if(TurretHotKey.lastActive){
                TurretHotKey.thkShader.setShaderInterface(this);
                TurretHotKey.thkShader.load();
                TurretHotKey.onSprite.draw();
                TurretHotKey.thkShader.unload();
            }else{
                TurretHotKey.offSprite.draw();

            }

        }
        orientate(ORIENTATION_BOTTOM | ORIENTATION_HORIZONTAL_MIDDLE);
        float x = getPos().x;
        float y = getPos().y;
        setPos(x -361, y - 30, 0);
        TurretHotKey.onSprite.setPos(getPos());
        TurretHotKey.offSprite.setPos(getPos());
    }

    float ticks = 0;
    @Override
    public void update(Timer timer) {
        super.update(timer);
        ticks+=0.01F;
    }

    @Override
    public void onInit() {

    }
    @Override
    public float getWidth() {
        return 16;
    }

    @Override
    public float getHeight() {
        return 16;
    }
    @Override
    public void onExit() {

    }

    @Override
    public void updateShader(DrawableScene scene) {

    }

    @Override
    public void updateShaderParameters(Shader shader) {
        //		uniform float uTime;
        //		uniform vec2 uResolution;
        //		uniform sampler2D uDiffuseTexture;

        GlUtil.updateShaderFloat(shader, "uTime", ticks);
        GlUtil.updateShaderVector2f(shader, "uResolution", 20, 1000);
        GlUtil.updateShaderInt(shader, "uDiffuseTexture", 0);

    }
}
