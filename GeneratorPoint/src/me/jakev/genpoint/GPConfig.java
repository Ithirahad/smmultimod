package me.jakev.genpoint;

import api.mod.config.FileConfiguration;

import java.util.Locale;

/**
 * Created by Jake on 10/23/2021.
 * <insert description here>
 */
public enum GPConfig {
    ITEM_RESTOCK_TICK_MS(1000),
    ITEM_AMOUNT(500),
    ;

    private int defaultVal;
    private int value;
    GPConfig(int defaultVal) {
        this.defaultVal = defaultVal;
    }
    public int getValue(){
        return value;
    }
    public static void init(GenPoint gp){
        FileConfiguration config = gp.getConfig("config");
        for (GPConfig value : GPConfig.values()) {
            value.value = config.getConfigurableInt(value.getConfigName(), value.defaultVal);
        }
        config.saveConfig();
    }
    public String getConfigName(){
        return name().toLowerCase(Locale.ENGLISH);
    }
}
